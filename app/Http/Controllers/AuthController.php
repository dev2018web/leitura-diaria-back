<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Token;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email' => $attr['email']
        ]);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'whatsapp' => 'required|string',
        ]);

        $user = User::where('whatsapp', $request->whatsapp)->first();
        
        $token = $user->createToken($request->whatsapp);

        return ['token' => $token->plainTextToken];
    }

    public function logout(Request $request)
    {   
        try{
            $attr = $request->validate([
                'token' => 'required|string',
            ]);

            $token = Token::where('token', '=', $request->get('token'))->firstOrFail();

            Token::where('name', $token->name)->delete();

            return [
                'message' => 'Tokens Revoked'
            ];
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $ex){
            return ['message' => 'Você não está logado!'];
        }catch(Illuminate\Validation\ValidationException $ex){
            dd(1);
        }catch(Exception $ex){
            return ['message' => $ex];
        }
    }
}